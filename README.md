Masume
======

A fork of [Bulmaswatch](https://jenil.github.io/bulmaswatch/) `Cosmo` theme.

## Reference

Original codes are placed in `source` branch.

## Credits

- @jgthms for [Bulma](http://bulma.io)
- @thomaspark for [Bootswatch](http://bootswatch.com/)
- @jenil for [Bulmaswatch](https://jenil.github.io/bulmaswatch/)

## License

Code released under the MIT License.
